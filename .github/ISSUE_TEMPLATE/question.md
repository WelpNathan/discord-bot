---
name: Questions / Help
label: ':speech_balloon: Question'
about: Questions aren't to be asked here
---

## Questions and Help

### Please note that this issue tracker is not a help forum and this issue will be closed.

For questions or help please see:

- [StackOverflow](https://stackoverflow.com/questions/ask)
