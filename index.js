/* eslint-disable global-require */
/* eslint-disable no-console */

/**
 * discord-bot
 * LICENSE MIT
 */

// Get all required environment variables.
const { DISCORD_BOT_APPLICATION_TOKEN } = process.env;

/**
 * Checks all of the environment variables are set before
 * requiring the main application file. The file runs all
 * of the setup.
 */
if (!DISCORD_BOT_APPLICATION_TOKEN) {
  console.error('Environment variables were not set. Consult README.md for more information.');
} else {
  require('./src/app');
}
