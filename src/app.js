/**
 * discord-bot
 * LICENSE MIT
 */

// Module dependencies
const path = require('path');
const Commando = require('discord.js-commando');

// Get helper functions.
const { setClientPresence } = require('./helpers/clientHelper');

// Get configuration files.
const botConfig = require('../config/bot');

/**
 * Get all required environment variables needed for
 * the bot to function as intended.
 *
 * DISCORD_BOT_APPLICATION_TOKEN is the token supplied
 * via the Discord developer portal.
 * DISCORD_BOT_DEVELOPER_ID is the administrators
 */
const { DISCORD_BOT_APPLICATION_TOKEN } = process.env;

/**
 * Creates a new Commando Client. This is an extended
 * version of the discord.js library.
 */
const client = new Commando.Client({
  owner: botConfig.applicationOwners,
  commandPrefix: botConfig.commandPrefix,
});

/**
 * Initialises the client registry. The registry is how Commando
 * understands where the commands, groups and other essential
 * things need to be configured.
 */
client.registry
  .registerGroups(botConfig.commandGroups)
  .registerDefaults()
  .registerCommandsIn(path.join(__dirname, 'commands'));

/**
 * Logs onto the Discord API. If it is successful, it will
 * set the client presence. If not, it will output that there
 * was an issue.
 */
client.login(DISCORD_BOT_APPLICATION_TOKEN)
  .then(() => setClientPresence(client, botConfig.presence))
  .catch(() => console.log('Client could not login to the Discord API. Is your bot token correct? Is the Discord API online?'));

module.exports = client;
