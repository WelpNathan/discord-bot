/* eslint-disable class-methods-use-this */

/**
 * discord-bot
 * LICENSE MIT
 */

// Module dependencies.
const commando = require('discord.js-commando');

// Initialise command via commando.Command
module.exports = class BanUserCommand extends commando.Command {
  constructor(client) {
    super(client, {
      name: 'ban',
      group: 'mod',
      memberName: 'ban',
      description: 'Bans a player from the guild.',
      examples: ['kick @player Spamming the guild'],
      guildOnly: true,

      clientPermissions: ['BAN_MEMBERS'],

      args: [
        {
          key: 'player',
          prompt: 'Ping the member to ban from the guild.',
          type: 'member',
        },
        {
          key: 'reason',
          prompt: 'Enter your reasons for banning the member.',
          type: 'string',
        },
      ],
    });
  }

  /**
   * Run the command once all arguments have been
   * entered into the command.
   *
   * @param {msg} string original message
   * @param {args} array contains all arguments
   */
  async run(msg, args) {
    const playerToBan = args.player;
    const reasonForBan = args.reason;

    playerToBan.ban(reasonForBan)
      .then(() => msg.reply(`${playerToBan} was successfully banned from the guild.`))
      .catch(() => msg.reply(`${playerToBan} could not be banned from the guild.`));
  }
};
