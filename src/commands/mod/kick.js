/* eslint-disable class-methods-use-this */

/**
 * discord-bot
 * LICENSE MIT
 */

// Module dependencies.
const commando = require('discord.js-commando');

// Initialise command via commando.Command
module.exports = class KickUserCommand extends commando.Command {
  constructor(client) {
    super(client, {
      name: 'kick',
      group: 'mod',
      memberName: 'kick',
      description: 'Kicks a player from the guild.',
      examples: ['kick @player Spamming the guild'],
      guildOnly: true,

      clientPermissions: ['KICK_MEMBERS'],

      args: [
        {
          key: 'player',
          prompt: 'Ping the member to kick from the guild.',
          type: 'member',
        },
        {
          key: 'reason',
          prompt: 'Enter your reasons for kicking the member.',
          type: 'string',
        },
      ],
    });
  }

  /**
   * Run the command once all arguments have been
   * entered into the command.
   *
   * @param {msg} string original message
   * @param {args} array contains all arguments
   */
  async run(msg, args) {
    const playerToKick = args.player;
    const reasonForKick = args.reason;

    playerToKick.kick(reasonForKick)
      .then(() => msg.reply(`${playerToKick} was successfully kicked from the guild.`))
      .catch(() => msg.reply(`${playerToKick} could not be kicked from the guild.`));
  }
};
