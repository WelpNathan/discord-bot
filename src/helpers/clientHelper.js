/**
 * discord-bot
 * LICENSE MIT
 */

/**
 * Function will connect to the Discord API and will update the
 * presence. This is done via a presence object in discord.js.
 * Outputs if successful or not.
 *
 * @param {PresenceData} presence
 * @returns {Promise}
 */
const setClientPresence = function setClientPresence(client, presence) {
  return new Promise(((resolve, reject) => {
    client.user.setPresence(
      { game: { name: presence.title, type: presence.type }, status: presence.status },
    )
      .then(resolve)
      .catch(reject);
  }))
    .then(() => console.log('Successfully set prescence via Discord API.'))
    .catch(() => console.error('Could not set the presence via the Discord API.'));
};

module.exports = { setClientPresence };
