/**
 * discord-bot
 * LICENSE MIT
 */

// Export all bot configurations.
module.exports = {
  presence: {
    title: 'discord-bot made by WelpNathan#0001',
    type: 'PLAYING', // https://discord.js.org/#/docs/main/stable/typedef/ActivityType
    status: 'online', // https://discord.js.org/#/docs/main/stable/typedef/PresenceStatus
  },
  commandGroups: [
    ['mod', 'Moderation Commands'],
  ],
  commandPrefix: '!',
  applicationOwners: ['218771421873045504'],
};
