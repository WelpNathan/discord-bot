# Get current stable node
FROM node:10.15.3-alpine

# Set essential environment variables
ENV NODE_ENV production
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

# Set working directory of image
WORKDIR /usr/src/app

# Copy package files for cache
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]

# Install NPM packages
RUN npm install --production --silent -g nodemon
RUN npm install --production --silent && mv node_modules ../

# Copy source code to working directory
COPY . .

# Start application
CMD npm start

# Use non-root user for security reasons
USER node