# Welcome to discord-bot!

<div align="center"><img src="https://raw.githubusercontent.com/WelpNathan/discord-bot/development/.github/readme-image.png" /></div>
  
<p align="center">
    <a href="https://david-dm.org/welpnathan/discord-bot" alt="Dependency Status">
        <img src="https://david-dm.org/welpnathan/discord-bot/status.svg" />
    </a>
    <a href="https://gitlab.com/WelpNathan/discord-bot/pipelines" alt="Pipeline Status">
        <img src="https://img.shields.io/gitlab/pipeline/welpnathan/discord-bot.svg" />
    </a>
    <a href="https://github.com/WelpNathan/discord-bot/issues" alt="GitHub Issues">
        <img src="https://img.shields.io/github/issues-raw/welpnathan/discord-bot.svg" />
    </a>
    <a href="https://github.com/WelpNathan/discord-bot/releases" alt="GitHub Release">
        <img src="https://img.shields.io/github/release/welpnathan/discord-bot.svg" />
    </a>
    <a href="https://lgtm.com/projects/g/WelpNathan/discord-bot/context:javascript" alt="JavaScript Code Rating">
        <img src="https://img.shields.io/lgtm/grade/javascript/g/WelpNathan/discord-bot.svg?logo=lgtm&logoWidth=18" />
    </a>
    <a href="https://lgtm.com/projects/g/WelpNathan/discord-bot/alerts/" alt="JavaScript Alert Rating">
        <img src="https://img.shields.io/lgtm/alerts/g/WelpNathan/discord-bot.svg" />
    </a>
    <a href="https://snyk.io/test/github/WelpNathan/discord-bot?targetFile=package.json" alt="Vunlnerability Status">
        <img src="https://snyk.io/test/github/WelpNathan/discord-bot/badge.svg?targetFile=package.json" />
    </a>
</p>

This is my first ever open-source code repository. I was inspired to write this as an aspiring software engineer. I'm more than happy to accept pull requests to further enhance the code. The bot contains every Discord command you could imagine. Discord is a platform for gamers to communicate and allows some sweet customisation options for your servers!

# Setting up the project

First things first, you need [Docker](https://www.docker.com/get-started) installed on your machine. Docker allows for cross-platform projects to work on MacOS, Linux and Windows. Some of you may choose to host your bot on your local machine, some may choose to host it on a small server and then some of you could even host it on huge enterprise servers. This code will work anywhere with Docker installed on a system. Still not convinced? Read [this](https://www.docker.com/why-docker).

## Running the project

After you've cloned the project, you need to create a `.env` file in your root project directory. These files follow the .env syntax so if you don't understand how to use this syntax, Google it, it's very simple! In this file, you need to add the following variables: `DISCORD_BOT_APPLICATION_TOKEN`.
How to get a [developer token](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token).

After you have added your environment variables in your `.env` file, boot up the project with this simple command `docker-compose up -d --build`! Docker will do everything for you, even the networking! To test that the project is working, go to a server which you have added your bot to and type `ping` prefixed by your command prefix. It should return a simple message with the bots heartbeat. If this command doesn't work, you've probably specified your token incorrectly. Research docker-compose logs in order to find what happened in the `container`.

# Current Commands
To ensure that the bot is as in-depth as possible, we've included tons of commands for you to use. If you don't like a specific group of commands, you can easily disable them.
All of the commands listed below use the `!` as a prefix, however, you may change this in `config/bot.js` along with many other settings!

## In-Built Commands
This project implements the `Discord.js-commando` library which is known for it's extensive suite of tools for making complex commands. All of these are enabled by default and **should not be disabled**. Taken from [discord.js commando](https://discord.js.org/#/docs/commando/master/commands/builtins) in-build command library.

| Command | Command Usage | Command Explanation |
|--|--|--|
| help | !help (cmd) | If no arguments are specified, the command displays a list of all commands available in the current context. In order for a command to be displayed, it must be enabled in the guild (or global), and the user must have permission to use it. Passing the `all` argument will list all commands, regardless of context. Passing anything else will search for any commands that match, and display detailed information if only one is found.
| ping | !ping | The ping command will send a message, then edit it to contain the amount of time it took. It also displays the client's heartbeat ping.
| prefix | !prefix | This command, if not provided with any arguments, will display the current command prefix, and how to use commands. If the command is used in a guild channel and an argument is specified, it will set the command prefix for the guild if the user is a guild admin, or the bot owner, If the command is used in a DM and an argument is specified, it will set the global default command prefix if the user is the bot owner.
| eval | !eval (code) | The eval command will allow the bot owner to evaluate any JavaScript code, and display its result. It will automatically hide the bot's token/email/password in the output. Caution should still be taken, however, as you could potentially break your running bot with it.
| enable | !enable (cmd/cmd group) | Enables a command/group in the current guild if the user is an admin or the bot owner. If used in a DM, enables the command/group globally by default if the user is the bot owner.
| disable | !disable (cmd/cmd group) | Disables a command/group in the current guild if the user is an admin or the bot owner. If used in a DM, disables the command/group globally by default if the user is the bot owner.
| reload | !reload (cmd/cmd group) | Reloads a command, or all commands in a group, if the user is the bot owner.
| load | !load (cmd/cmd group) | Loads a command if the user is the bot owner. The command must be specified as the full name (`group:memberName`). Built-in commands cannot be loaded.
| unload | !unload (cmd/cmd group) | Unloads a command if the user is the bot owner. Built-in commands cannot be unloaded.
| groups | !groups | Lists all command groups if the user is an admin of the current guild, or the bot owner.

## Moderation Commands
Moderation commands are how you can easily manage a Discord community. We include tons of moderation commands, `ban` and `kick` are just the beginning. We have tons more in the making.

| Command | Command Usage | Command Explanation |
|--|--|--|
| ban | !ban (member) (reason) | The ban command will allow anyone with the `BAN_MEMBERS` permission to ban a member from a guild.
| kick | !kick (member) (reason) | The kick command will allow anyone with the `KICK_MEMBERS` permission to kick a member from a guild.